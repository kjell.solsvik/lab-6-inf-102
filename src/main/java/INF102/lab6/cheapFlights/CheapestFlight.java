package INF102.lab6.cheapFlights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> g = new WeightedDirectedGraph<>();
        for (Flight flight : flights) {
            City start = flight.start;
            City dest = flight.destination;
            int cost = flight.cost;

            g.addVertex(start);
            g.addVertex(dest);
            g.addEdge(start, dest, cost);
        }
        return g;
        
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> g = constructGraph(flights);
        Map<City, Integer> prices = dijkstra(g, start, nMaxStops);
        return prices.get(destination);
    }

    
    private Map<City, Integer> dijkstra(WeightedDirectedGraph<City, Integer> g, City start, int nMaxStops) {
        Map<ReachInNStops, Trip> bestPrice = new HashMap<>();

        PriorityQueue<Trip> q = new PriorityQueue<>();
        ReachInNStops startReach = new ReachInNStops(start, 0);
        Trip startTrip = new Trip(startReach, 0);
        addNeighbours(startTrip, g, q);
        bestPrice.put(startReach, startTrip);

        while(!q.isEmpty()) {
            Trip current = q.remove();
            if (bestPrice.containsKey(current.destInNStops)) {
                continue;
            }
            bestPrice.put(current.destInNStops, current);
            if (current.destInNStops.nStops <= nMaxStops) {
                addNeighbours(current, g, q);
            }
        }

        Map<City, Integer> distances = new HashMap<>();
        for (Trip trip : bestPrice.values()) {
            City city = trip.destInNStops.destination;
            int price = trip.totalPrice;
            if (trip.destInNStops.nStops > nMaxStops +1) {
                continue;
            }
            if (!distances.containsKey(city) || distances.get(city) > price) {
                distances.put(city, price);
            }
        }
        return distances;
    }

    private void addNeighbours(Trip currentTrip, WeightedDirectedGraph<City, Integer> g, PriorityQueue<Trip> q) {
        for (City n : g.outNeighbours(currentTrip.destInNStops.destination)) {
            ReachInNStops currentReach = new ReachInNStops(n, currentTrip.destInNStops.nStops +1);
            Trip trip = new Trip(currentReach, currentTrip.totalPrice + g.getWeight(currentTrip.destInNStops.destination, n));
            q.add(trip);
        }
    }

    class ReachInNStops {
        City destination;
        Integer nStops;

        public ReachInNStops(City dest, Integer nStops ) {
            this.destination = dest;
            this.nStops = nStops;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof ReachInNStops)) {
                return false;
            }
            ReachInNStops other = (ReachInNStops) o;
            return this.destination.equals(other.destination) && this. nStops.equals(other.nStops);
        }

        @Override
        public int hashCode() {
            return Objects.hash(destination, nStops);
        }
    }

    class Trip implements Comparable<Trip> {
        ReachInNStops destInNStops;
        Integer totalPrice;

        public Trip(ReachInNStops reach, Integer price) {
            this.destInNStops = reach;
            this.totalPrice = price;
        }

        @Override
        public int compareTo(Trip o) {
            return Integer.compare(totalPrice, o.totalPrice);
        }

    }

    
}
